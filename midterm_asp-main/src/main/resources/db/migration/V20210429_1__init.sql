
DROP TABLE IF EXISTS user;
CREATE TABLE user(
    id serial,
    name VARCHAR(255)
);

DROP TABLE IF EXISTS chat;
CREATE TABLE chat(
    id serial,
    name VARCHAR(255)
);

DROP TABLE IF EXISTS message;
CREATE TABLE message(
    id serial,
    user_id BIGINT,
    chat_id BIGINT,
    text VARCHAR(255)
);


DROP TABLE IF EXISTS participant;
CREATE TABLE participant(
    id serial,
    user_id BIGINT,
    chat_id BIGINT
);

