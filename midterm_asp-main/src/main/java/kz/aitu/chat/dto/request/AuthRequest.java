package kz.aitu.chat.dto.request;

import kz.aitu.chat.model.Role;
import lombok.Data;

import java.util.Set;

@Data
public class AuthRequest {
    private String username;
    private String password;
    private UserDtoRequest user;
    private Set<Role> roles;
}
