package kz.aitu.chat.dto.response;

import kz.aitu.chat.model.Auth;
import kz.aitu.chat.model.AuthToken;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoginResponse {
    private AuthToken token;
    private Auth auth;
}
