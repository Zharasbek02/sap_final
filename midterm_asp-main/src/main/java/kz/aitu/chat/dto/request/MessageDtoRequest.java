package kz.aitu.chat.dto.request;

import kz.aitu.chat.model.MessageType;
import lombok.Data;

import java.util.Date;

@Data
public class MessageDtoRequest {
    private Long id;
    private String text;
    private Long userId;
    private Long chatId;
}
