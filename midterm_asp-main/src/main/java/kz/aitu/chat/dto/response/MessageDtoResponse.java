package kz.aitu.chat.dto.response;

import kz.aitu.chat.model.MessageType;
import lombok.Data;

import java.util.Date;

@Data
public class MessageDtoResponse {
    private Long id;
    private String text;
    private UserDtoResponse user;
    private ChatDtoResponse chat;
}
