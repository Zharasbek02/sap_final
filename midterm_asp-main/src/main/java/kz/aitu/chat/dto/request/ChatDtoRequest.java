package kz.aitu.chat.dto.request;

import lombok.Data;

import java.util.List;

@Data
public class ChatDtoRequest {
    private Long id;
    private String name;
}
