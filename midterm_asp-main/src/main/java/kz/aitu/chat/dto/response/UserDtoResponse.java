package kz.aitu.chat.dto.response;

import kz.aitu.chat.model.Status;
import lombok.Data;

@Data
public class UserDtoResponse {
    private Long id;
    private String name;
}
