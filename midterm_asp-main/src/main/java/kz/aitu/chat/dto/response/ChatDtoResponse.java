package kz.aitu.chat.dto.response;

import lombok.Data;

import java.util.List;

@Data
public class ChatDtoResponse {
    private Long id;
    private String name;
}
