package kz.aitu.chat.dto.request;

import kz.aitu.chat.model.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class UserDtoRequest {
    private Long id;
    private String name;
}
