package kz.aitu.chat.repository;

import kz.aitu.chat.model.Auth;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthRepository extends JpaRepository<Auth,Long> {
    Auth findByUsername(String username);
}
