package kz.aitu.chat.utils.facade;

import kz.aitu.chat.dto.response.MessageDtoResponse;
import kz.aitu.chat.model.Message;

import java.util.ArrayList;
import java.util.List;

public class MessageFacade {
    public static MessageDtoResponse messageToMessageDtoResponse(Message message) {
        MessageDtoResponse messageDtoResponse = new MessageDtoResponse();
        if(message.getId()!=null) {
            messageDtoResponse.setId(message.getId());
        }
        if(message.getText()!=null) {
            messageDtoResponse.setText(message.getText());
        }
        if(message.getUser()!=null) {
            messageDtoResponse.setUser(UserFacade.userToUserDtoResponse(message.getUser()));
        }
        if(message.getChat()!=null) {
            messageDtoResponse.setChat(ChatFacade.chatToChatToDtoResponse(message.getChat()));
        }
        return messageDtoResponse;
    }

    public static List<MessageDtoResponse> messageListToMessageDtoResponseList(List<Message> messages) {
        List<MessageDtoResponse> messageDtoResponses = new ArrayList<>();
        for(Message message: messages) {
            messageDtoResponses.add(messageToMessageDtoResponse(message));
        }
        return messageDtoResponses;
    }
}
