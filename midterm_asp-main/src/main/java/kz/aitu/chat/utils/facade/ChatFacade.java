package kz.aitu.chat.utils.facade;

import kz.aitu.chat.dto.response.ChatDtoResponse;
import kz.aitu.chat.model.Chat;

import java.util.ArrayList;
import java.util.List;

public class ChatFacade {
    public static ChatDtoResponse chatToChatToDtoResponse(Chat chat) {
        ChatDtoResponse chatDtoResponse = new ChatDtoResponse();
        if(chat.getId()!=null) {
            chatDtoResponse.setId(chat.getId());
        }
        if(chat.getName()!=null) {
            chatDtoResponse.setName(chat.getName());
        }
        return chatDtoResponse;
    }

    public static List<ChatDtoResponse> chatListToChatDtoResponseList(List<Chat> chats) {
        List<ChatDtoResponse> chatDtoResponses = new ArrayList<>();
        for(Chat chat:chats) {
            chatDtoResponses.add(chatToChatToDtoResponse(chat));
        }
        return chatDtoResponses;
    }
}
