package kz.aitu.chat.utils.facade;

import kz.aitu.chat.dto.request.UserDtoRequest;
import kz.aitu.chat.dto.response.UserDtoResponse;
import kz.aitu.chat.exceptions.ConflictException;
import kz.aitu.chat.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserFacade {
    public static UserDtoResponse userToUserDtoResponse(User user) {
        UserDtoResponse userDtoResponse = new UserDtoResponse();
        userDtoResponse.setId(user.getId());
        userDtoResponse.setName(user.getName());
        return userDtoResponse;
    }
    public static User userDtoRequestToUser(UserDtoRequest userDtoRequest) {
        User user = new User();
        if(userDtoRequest.getId()!=null) {
            user.setId(userDtoRequest.getId());
        }
        if(userDtoRequest.getName()!=null) {
            user.setName(userDtoRequest.getName());
        }
        return user;
    }
    public static List<UserDtoResponse> userListToUserDtoResponseList(List<User> users) {
        List<UserDtoResponse> userDtoResponses = new ArrayList<>();
        for(User user: users) {
            userDtoResponses.add(userToUserDtoResponse(user));
        }
        return userDtoResponses;
    }
}
