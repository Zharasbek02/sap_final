package kz.aitu.chat.service;

import kz.aitu.chat.dto.request.ChatDtoRequest;
import kz.aitu.chat.dto.request.UserDtoRequest;
import kz.aitu.chat.model.Chat;
import kz.aitu.chat.model.User;
import kz.aitu.chat.repository.ChatRepository;
import kz.aitu.chat.repository.UserRepository;
import kz.aitu.chat.utils.facade.UserFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class ChatService {
    @Autowired
    private ChatRepository repository;

    public Chat add(ChatDtoRequest chatDtoRequest) {
        Chat chat = new Chat();
        chat.setName(chatDtoRequest.getName());
        return save(chat);
    }

    public Chat save(Chat chat) {
        return repository.save(chat);
    }

    public Chat get(Long id) {
        return repository.findById(id).get();
    }


    public boolean existsById(Long id) {
        return repository.existsById(id);
    }

    public List<Chat> findAll(){
        return repository.findAll();
    }

    public void deleteById(Long id){
        repository.deleteById(id);
    }

}
