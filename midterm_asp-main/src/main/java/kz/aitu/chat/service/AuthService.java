package kz.aitu.chat.service;

import kz.aitu.chat.dto.request.AuthRequest;
import kz.aitu.chat.model.Auth;
import kz.aitu.chat.model.Role;
import kz.aitu.chat.model.User;
import kz.aitu.chat.repository.AuthRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Date;
import java.util.Set;

@Service(value = "authService")
public class AuthService implements UserDetailsService{
    @Autowired
    private UserService userService;

    @Autowired
    private AuthRepository authRepository;

    @Autowired
    private BCryptPasswordEncoder bcryptEncoder;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Auth auth = authRepository.findByUsername(s);
        if(auth == null){
            throw new kz.aitu.chat.exceptions.UsernameNotFoundException("Invalid username or password.");
        }
        return auth;
    }

    public Auth add(AuthRequest authRequest) {
        Auth auth = new Auth();
        User user = userService.add(authRequest.getUser());
        auth.setUser(user);
        auth.setRoles(Collections.singleton(Role.USER));
        auth.setUsername(authRequest.getUsername());
        auth.setPassword(passwordEncoder.encode(authRequest.getPassword()));
        auth.setEnabled(true);
        return save(auth);
    }

    public Auth save(Auth auth) {
        return authRepository.save(auth);
    }

    public Boolean existsById(Long id) {
        return authRepository.existsById(id);
    }

    public Auth get(Long id) {
        return authRepository.findById(id).get();
    }

    public Auth getByUsername(String username){
        return authRepository.findByUsername(username);
    }

    public String getToken(String username) {
        return getByUsername(username).getToken();
    }

    public Auth updateToken(Long id, String token) {
        Auth auth = get(id);
        auth.setToken(token);
        return save(auth);
    }

    public Auth logined(Long id) {
        Auth auth = get(id);
        return save(auth);
    }
}
