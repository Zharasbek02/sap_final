package kz.aitu.chat.service;

import kz.aitu.chat.dto.request.MessageDtoRequest;
import kz.aitu.chat.model.Auth;
import kz.aitu.chat.model.Chat;
import kz.aitu.chat.model.Message;
import kz.aitu.chat.model.User;
import kz.aitu.chat.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class MessageService {
    @Autowired
    private MessageRepository repository;
    @Autowired
    private UserService userService;
    @Autowired
    private ChatService chatService;


    public Message add(MessageDtoRequest messageDtoRequest, Auth auth) {
        Chat chat = chatService.get(messageDtoRequest.getChatId());
        Message message = new Message();
        message.setChat(chat);
        message.setUser(auth.getUser());
        message.setText(messageDtoRequest.getText());
        return save(message);
    }

    public Message getById(Long id) {
        Message message = repository.findById(id).get();
        return save(message);
    }


    public List<Message> getAll() {
        return repository.findAll();
    }

    public Message update(Long id, MessageDtoRequest messageDtoRequest) {
        Message message = getById(id);
        message.setText(messageDtoRequest.getText());
        return save(message);
    }

    public boolean existsById(Long id) {
        return repository.existsById(id);
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    public Message save(Message message) {
        return repository.save(message);
    }
}
