package kz.aitu.chat.service;

import kz.aitu.chat.dto.request.UserDtoRequest;
import kz.aitu.chat.model.Status;
import kz.aitu.chat.model.User;
import kz.aitu.chat.repository.UserRepository;
import kz.aitu.chat.utils.facade.UserFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    public User add(UserDtoRequest userDtoRequest) {
        User user = UserFacade.userDtoRequestToUser(userDtoRequest);
        return save(user);
    }

    public List<User> getAll() {
        return repository.findAll();
    }

    public User getById(Long id) {
        return repository.findById(id).get();
    }

    public boolean existsById(Long id) {
        return repository.existsById(id);
    }

    public User save(User user) {
        return repository.save(user);
    }

    public User update(UserDtoRequest userDtoRequest) {
        User user = getById(userDtoRequest.getId());
        user.setName(userDtoRequest.getName());
        return save(user);
    }


    public void delete(Long id) {
        repository.deleteById(id);
    }

}
