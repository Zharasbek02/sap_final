package kz.aitu.chat.controller;

import kz.aitu.chat.dto.request.ChatDtoRequest;
import kz.aitu.chat.exceptions.ResourceNotFoundException;
import kz.aitu.chat.service.ChatService;
import kz.aitu.chat.utils.facade.ChatFacade;
import kz.aitu.chat.utils.facade.UserFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/chats")
public class ChatController {

    @Autowired
    private ChatService service;

    @GetMapping()
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(ChatFacade.chatListToChatDtoResponseList(service.findAll()));
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        return ResponseEntity.ok(ChatFacade.chatToChatToDtoResponse(service.get(id)));
    }

    @PutMapping
    public ResponseEntity<?> addChat(@RequestBody ChatDtoRequest chatDtoRequest){
        return ResponseEntity.ok(ChatFacade.chatToChatToDtoResponse(service.add(chatDtoRequest)));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteChat(@PathVariable Long id){
        if(!service.existsById(id)) {
            throw new ResourceNotFoundException("Chat not found");
        }
        service.deleteById(id);
        return ResponseEntity.ok("chat successfully deleted");
    }

}
