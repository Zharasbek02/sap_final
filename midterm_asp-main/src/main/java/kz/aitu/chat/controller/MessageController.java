package kz.aitu.chat.controller;

import kz.aitu.chat.dto.request.MessageDtoRequest;
import kz.aitu.chat.exceptions.ResourceNotFoundException;
import kz.aitu.chat.model.Auth;
import kz.aitu.chat.repository.MessageRepository;
import kz.aitu.chat.model.Message;
import kz.aitu.chat.service.ChatService;
import kz.aitu.chat.service.MessageService;
import kz.aitu.chat.service.UserService;
import kz.aitu.chat.utils.facade.MessageFacade;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("/api/v1/message")
public class MessageController {
    @Autowired
    private MessageService service;

    @Autowired
    private ChatService chatService;

    @Autowired
    private UserService userService;

    @GetMapping
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(MessageFacade.messageListToMessageDtoResponseList(service.getAll()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable Long id){
        if(!service.existsById(id)){
            throw new ResourceNotFoundException("message not found");
        }
        return ResponseEntity.ok(MessageFacade.messageToMessageDtoResponse(service.getById(id)));
    }
    @PutMapping
    public ResponseEntity<?> add(@RequestBody MessageDtoRequest message, @AuthenticationPrincipal Auth auth){
        return ResponseEntity.ok(MessageFacade.messageToMessageDtoResponse(service.add(message, auth)));
    }

    @PostMapping("/{id}")
    public ResponseEntity<?> updateMessage(@PathVariable Long id, @RequestBody MessageDtoRequest message){
        if(!service.existsById(id)){
            throw new ResourceNotFoundException("message not found");
        }
        return ResponseEntity.ok(MessageFacade.messageToMessageDtoResponse(service.update(id,message)));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteMessage(@PathVariable Long id){
        if(!service.existsById(id)){
            throw new ResourceNotFoundException("message not found");
        }
        service.deleteById(id);
        return ResponseEntity.ok("message successfully deleted");
    }

}
