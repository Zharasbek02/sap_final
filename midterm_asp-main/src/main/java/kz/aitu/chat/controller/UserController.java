package kz.aitu.chat.controller;

import kz.aitu.chat.dto.request.UserDtoRequest;
import kz.aitu.chat.exceptions.ConflictException;
import kz.aitu.chat.exceptions.ResourceNotFoundException;
import kz.aitu.chat.model.Status;
import kz.aitu.chat.service.UserService;
import kz.aitu.chat.utils.facade.UserFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {
    @Autowired
    private UserService service;

    @GetMapping
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(UserFacade.userListToUserDtoResponseList(service.getAll()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable Long id){
        if(!service.existsById(id)){
            throw new ResourceNotFoundException("user not found with id: "+id);
        }
        return ResponseEntity.ok(UserFacade.userToUserDtoResponse(service.getById(id)));
    }

    @PutMapping
    public ResponseEntity<?> add(@RequestBody UserDtoRequest userDtoRequest){
        if(userDtoRequest.getName()==null) {
            throw new ConflictException("there are no user name data");
        }
        return ResponseEntity.ok(UserFacade.userToUserDtoResponse(service.add(userDtoRequest)));
    }

    @PostMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody UserDtoRequest userDtoRequest) {
        if(!service.existsById(id)) {
            throw new ResourceNotFoundException("user not found with id: "+id);
        }
        return ResponseEntity.ok(UserFacade.userToUserDtoResponse(service.update(userDtoRequest)));
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id){
        if(!service.existsById(id)) {
            throw new ResourceNotFoundException("user not found with id: "+id);
        }
        service.delete(id);
        return ResponseEntity.ok("user successfully deleted");
    }

}
