package kz.aitu.chat.controller;

import kz.aitu.chat.dto.request.AuthRequest;
import kz.aitu.chat.dto.request.LoginUser;
import kz.aitu.chat.dto.response.LoginResponse;
import kz.aitu.chat.model.Auth;
import kz.aitu.chat.model.AuthToken;
import kz.aitu.chat.service.AuthService;
import kz.aitu.chat.utils.TokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {
    @Autowired
    private AuthService authService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenProvider jwtTokenUtil;

    @PutMapping
    public ResponseEntity<?> register(@RequestBody AuthRequest authRequest) {
        return ResponseEntity.ok(authService.add(authRequest));
    }

    @PostMapping
    public ResponseEntity<?> login(@RequestBody LoginUser loginUser) throws AuthenticationException {
        Auth auth = (Auth) authService.loadUserByUsername(loginUser.getUsername());
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginUser.getUsername(),
                        loginUser.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final String token = jwtTokenUtil.generateToken(authentication);
        authService.updateToken(auth.getId(), token);
        authService.logined(auth.getId());
        return ResponseEntity.ok(new LoginResponse(new AuthToken(token),auth));
    }
}
